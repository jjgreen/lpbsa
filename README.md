lpbsa
=======

Calculate the surface area of the *ℓ<sub>p</sub>* unit-ball in
small dimensions by a brute-force quadrature.  This seems to
work but is not optimised and only lightly tested, don't rely
on this.
