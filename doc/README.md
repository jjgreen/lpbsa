lpbsa documentation
--------------------

The docbook source in `lpbsa.xml` is used to generate the Unix
man-page `lpbsa.3` and the plain text `lpbsa.txt`. The tools
`xsltproc` and `lynx` are required to regenerate these files.

The script `lpbsa-fetch.sh` pulls the latest version of `lpbsa.c`
and `lpbsa.h` from the GitHub master to the current directory.
