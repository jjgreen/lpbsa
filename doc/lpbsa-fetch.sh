#!/bin/sh
#
# refresh lpbsa.* from git master

NAME=lpbsa
URL="https://gitlab.com/jjg/$NAME/raw/master"

for ext in c h
do
    FILE="$NAME.$ext"
    printf '%s ..' "$FILE"
    wget -q $URL/$FILE -O $FILE
    printf '. done\n'
done
