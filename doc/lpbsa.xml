<?xml version="1.0" encoding="UTF-8"?>
<refentry
    id='lpbsa3'
    xmlns='http://docbook.org/ns/docbook'
    version='5.0'
    xml:lang='en'>

  <refentryinfo><date>2018-04-02</date></refentryinfo>
  <refmeta>
    <refentrytitle>LPBSA</refentrytitle>
    <manvolnum>3</manvolnum>
    <refmiscinfo class='manual'>Library Functions Manual</refmiscinfo>
    <refmiscinfo class='source'>lpbsa</refmiscinfo>
    <refmiscinfo class='date'>2018-04-02</refmiscinfo>
  </refmeta>

  <refnamediv id='name'>
    <refname>lpbsa</refname>
    <refpurpose>
      calculate the surface area of the unit ball in
      n-dimensional
      <inlineequation>ℓ<subscript>p</subscript></inlineequation>
      space.
    </refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <funcsynopsis>
      <funcsynopsisinfo><![CDATA[#include <lpbsa.h>]]></funcsynopsisinfo>

      <funcprototype>
        <?dbhtml funcsynopsis-style='ansi'?>
        <funcdef>double <function>lpbsa</function></funcdef>
        <paramdef>double <parameter>p</parameter></paramdef>
        <paramdef>size_t <parameter>n</parameter></paramdef>
        <paramdef>size_t <parameter>m</parameter></paramdef>
      </funcprototype>

      <funcprototype>
        <?dbhtml funcsynopsis-style='ansi'?>
        <funcdef>double <function>lpbsa_long</function></funcdef>
        <paramdef>double <parameter>p</parameter></paramdef>
        <paramdef>size_t <parameter>n</parameter></paramdef>
        <paramdef>size_t <parameter>m</parameter></paramdef>
        <paramdef>lpbsa_opt_t * <parameter>opt</parameter></paramdef>
      </funcprototype>
    </funcsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>

    <para>
      The <function>lpbsa</function> function estimates the surface
      area of the unit ball with respect to the
      <varname>p</varname>-norm in
      <varname>n</varname>-dimensional space.  The method is a
      simple quadrature using <varname>m</varname> samples in
      each dimension (in fact fewer are used, since we can exploit
      symmetry).
    </para>

    <para>
      The <function>lpbsa_long</function> function is similar, but
      also has a <varname>opt</varname> argument which allows one
      a little more control over the quadrature, this is mainly
      intended for testing and development.
    </para>

    <para>
      The <varname>p</varname> argument should be in the range from
      one to <constant>INFINITY</constant>, but non-infinite values
      larger than around 60 will error due to overflow in calls to
      <function>pow</function>.
    </para>

    <para>
      The <varname>n</varname> argument should be at least two,
      and no greater than ten (in principle it could be greater
      than ten, but the complexity of the quadrature is
      exponential in this variable, so the computation is
      practical only up to five or six).
    </para>

    <para>
      The <varname>m</varname> argument should be at least one,
      and the accuracy of the estimate increases with
      <varname>m</varname>, so one would like a value as large
      as possible.  Around 100 is a reasonable starting value.
    </para>

    <para>
      The <varname>opt</varname> option is a pointer to a structure
      with members
    </para>

    <itemizedlist mark='opencircle'>
      <listitem>
        <para>
          <structfield>short_circuit</structfield>, a <type>bool</type>
          indicating that for <varname>p</varname> values of 1, 2 or
          <constant>INFINITY</constant>, exact formulae should be
          used instead of performing the quadrature.  This is
          <constant>true</constant> by default, but the
          <constant>false</constant> value is useful for tests.
        </para>
      </listitem>
    </itemizedlist>

    <para>
      The <varname>opt</varname> argument may be
      <constant>NULL</constant>, in which case reasonable defaults will
      be used (in fact the <function>lpbsa</function> function is
      implemented in this fashion).
    </para>
  </refsect1>

  <refsect1 id='return_value'>
    <title>RETURN VALUE</title>

    <para>
      On success the extimate of the surface are is returned.  On
      failure a negative value is returned and <varname>errno</varname>
      is set to indicate the cause of the failure.
    </para>

  </refsect1>

  <refsect1 id='author'>
    <title>AUTHORS</title>
    <para>J.J. Green.</para>
  </refsect1>

</refentry>
