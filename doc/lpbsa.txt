Name

   lpbsa — calculate the surface area of the unit ball in n-dimensional
   ℓ[p] space.

Synopsis

#include <lpbsa.h>

   double lpbsa( double p,
                 size_t n,
                 size_t m);

   double lpbsa_long( double p,
                      size_t n,
                      size_t m,
                      lpbsa_opt_t * opt);

DESCRIPTION

   The lpbsa function estimates the surface area of the unit ball with
   respect to the p-norm in n-dimensional space. The method is a simple
   quadrature using m samples in each dimension (in fact fewer are used,
   since we can exploit symmetry).

   The lpbsa_long function is similar, but also has a opt argument which
   allows one a little more control over the quadrature, this is mainly
   intended for testing and development.

   The p argument should be in the range from one to INFINITY, but
   non-infinite values larger than around 60 will error due to overflow in
   calls to pow.

   The n argument should be at least two, and no greater than ten (in
   principle it could be greater than ten, but the complexity of the
   quadrature is exponential in this variable, so the computation is
   practical only up to five or six).

   The m argument should be at least one, and the accuracy of the estimate
   increases with m, so one would like a value as large as possible.
   Around 100 is a reasonable starting value.

   The opt option is a pointer to a structure with members
     * short_circuit, a bool indicating that for p values of 1, 2 or
       INFINITY, exact formulae should be used instead of performing the
       quadrature. This is true by default, but the false value is useful
       for tests.

   The opt argument may be NULL, in which case reasonable defaults will be
   used (in fact the lpbsa function is implemented in this fashion).

RETURN VALUE

   On success the extimate of the surface are is returned. On failure a
   negative value is returned and errno is set to indicate the cause of
   the failure.

AUTHORS

   J.J. Green.
