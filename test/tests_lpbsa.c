/*
  tests_lpbsa.c

  Copyright (c) J.J. Green 2018
*/

#include "tests_lpbsa.h"
#include "tests_helper.h"

CU_TestInfo tests_lpbsa[] =
  {
    {"bad dimension",  test_lpbsa_bad_dimension},
    {"bad p", test_lpbsa_bad_p},
    {"2-tetrahedron (coarse)", test_lpbsa_diamond_coarse},
    {"2-tetrahedron (fine)", test_lpbsa_diamond_fine},
    {"2-sphere", test_lpbsa_disc},
    {"3-sphere", test_lpbsa_sphere},
    {"2-cube (coarse)", test_lpbsa_square_coarse},
    {"2-cube (fine)", test_lpbsa_square_fine},
    {"3-cube (coarse)", test_lpbsa_cube_coarse},
    {"3-cube (fine)", test_lpbsa_cube_fine},
    {"4-cube (coarse)", test_lpbsa_hypercube_coarse},
    {"4-cube (fine)", test_lpbsa_hypercube_fine},
    {"short-circuit 1", test_lpbsa_short_circuit_1},
    {"short-circuit 2", test_lpbsa_short_circuit_2},
    {"short-circuit infinity", test_lpbsa_short_circuit_inf},
    CU_TEST_INFO_NULL,
  };

static double lpbsa_test(double p, size_t n, size_t m)
{
  lpbsa_opt_t opt = {
    .short_circuit = false
  };
  return lpbsa_long(p, n, m, &opt);
}

static void check_bad_dimension(size_t n)
{
  errno = 0;
  CU_ASSERT(lpbsa_test(2, n, 20) < 0);
  CU_ASSERT_EQUAL(errno, EDOM);
  errno = 0;
}

extern void test_lpbsa_bad_dimension(void)
{
  check_bad_dimension(0);
  check_bad_dimension(1);
}

static void check_bad_p(double p)
{
  errno = 0;
  CU_ASSERT(lpbsa_test(p, 2, 20) < 0);
  CU_ASSERT_EQUAL(errno, EDOM);
  errno = 0;
}

extern void test_lpbsa_bad_p(void)
{
  check_bad_p(-INFINITY);
  check_bad_p(-3.0);
  check_bad_p(0.5);
  check_bad_p(NAN);
}

extern void test_lpbsa_diamond_coarse(void)
{
  errno = 0;
  double area = lpbsa_test(1, 2, 3);
  CU_ASSERT_DOUBLE_EQUAL(area, 4 * sqrt(2), 0.1);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_diamond_fine(void)
{
  errno = 0;
  double area = lpbsa_test(1, 2, 100);
  CU_ASSERT_DOUBLE_EQUAL(area, 4 * sqrt(2), 1e-4);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_disc(void)
{
  errno = 0;
  double area = lpbsa_test(2, 2, 100);
  CU_ASSERT_DOUBLE_EQUAL(area, 2 * M_PI, 1e-4);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_sphere(void)
{
  errno = 0;
  double area = lpbsa_test(2, 3, 100);
  CU_ASSERT_DOUBLE_EQUAL(area, 4 * M_PI, 5e-2);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_square_coarse(void)
{
  double area = lpbsa_test(INFINITY, 2, 2);
  errno = 0;
  CU_ASSERT_DOUBLE_EQUAL(area, 8, 1e-8);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_square_fine(void)
{
  errno = 0;
  double area = lpbsa_test(INFINITY, 2, 16);
  CU_ASSERT_DOUBLE_EQUAL(area, 8, 1e-8);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_cube_coarse(void)
{
  errno = 0;
  double area = lpbsa_test(INFINITY, 3, 2);
  CU_ASSERT_DOUBLE_EQUAL(area, 24, 1e-8);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_cube_fine(void)
{
  double area = lpbsa_test(INFINITY, 3, 16);
  errno = 0;
  CU_ASSERT_DOUBLE_EQUAL(area, 24, 1e-8);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_hypercube_coarse(void)
{
  errno = 0;
  double area = lpbsa_test(INFINITY, 4, 2);
  CU_ASSERT_DOUBLE_EQUAL(area, 64, 1e-8);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_hypercube_fine(void)
{
  errno = 0;
  double area = lpbsa_test(INFINITY, 4, 16);
  CU_ASSERT_DOUBLE_EQUAL(area, 64, 1e-8);
  CU_ASSERT_EQUAL(errno, 0);
}

static void check_short_circuit(double p, size_t n, size_t m, double eps)
{
  errno = 0;
  double
    area1 = lpbsa(p, n, m),
    area2 = lpbsa_test(p, n, m);
  CU_ASSERT_DOUBLE_EQUAL(area1, area2, eps);
  CU_ASSERT_EQUAL(errno, 0);
}

extern void test_lpbsa_short_circuit_1(void)
{
  check_short_circuit(1, 2, 1024, 1e-4);
  check_short_circuit(1, 3, 1024, 1e-4);
}

extern void test_lpbsa_short_circuit_2(void)
{
  check_short_circuit(2, 2,  128, 1e-4);
  check_short_circuit(2, 3, 1024, 5e-3);
}

extern void test_lpbsa_short_circuit_inf(void)
{
  check_short_circuit(INFINITY, 2, 1, 1e-8);
  check_short_circuit(INFINITY, 3, 1, 1e-8);
  check_short_circuit(INFINITY, 4, 1, 1e-8);
}
