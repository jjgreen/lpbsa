/*
  tests_helper.h

  Copyright (c) J.J. Green 2018
*/

#ifndef TESTS_HELPER_H
#define TESTS_HELPER_H

#include <math.h>
#include <errno.h>
#include <lpbsa.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

#endif
