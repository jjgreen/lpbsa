/*
  lpbsa.c

  J.J. Green 2018
*/

#include <math.h>
#include <stdlib.h>
#include <errno.h>

#include "lpbsa.h"

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

#define BAD_VALUE -1.0

static double dot(double *a, double *b, size_t n)
{
  double sum = 0.0;

  for (size_t i = 0 ; i < n ; i++)
    sum += a[i] * b[i];

  return sum;
}

static double pnorm_two(double *s, size_t n)
{
  return sqrt(dot(s, s, n));
}

static double pnorm_finite(double *s, size_t n, double p)
{
  double sum = 0.0;

  for (size_t i = 0 ; i < n ; i++)
    sum += pow(s[i], p);

  return pow(sum, 1 / p);
}

static double pnorm_infinite(double *s, size_t n, double p)
{
  double max = s[0];

  for (size_t i = 1 ; i < n ; i++)
    if (s[i] > max) max = s[i];

  return max;
}

static double pnorm(double *s, size_t n, double p)
{
  if (isinf(p)) return pnorm_infinite(s, n, p);
  if (p == 2) return pnorm_two(s, n);
  return pnorm_finite(s, n, p);
}

static double factorial(size_t n)
{
  static const int table[] = {
    1,
    1,
    2,
    6,
    24,
    120,
    720,
    5040,
    40320,
    362880,
    3628800
  };
  static const size_t
    entries = sizeof(table) / sizeof(int);

  if (n < entries)
    return table[n];

  return tgamma(n + 1);
}

/*
  We need the Eulerian numbers of order the dimension of
  the lp-space, which realistically will be 5 or 6 at most,
  so rather than implementing a generic calculation we just
  have a lookup table
*/

static int eulerian_lookup(size_t n, size_t m)
{
  static const int table[] = {
    1,
    1,    1,
    1,    4,     1,
    1,   11,    11,      1,
    1,   26,    66,     26,       1,
    1,   57,   302,    302,      57,       1,
    1,  120,  1191,   2416,    1191,     120,      1,
    1,  247,  4293,  15619,   15619,    4293,    247,     1,
    1,  502, 14608,  88234,  156190,   88234,  14608,   502,    1,
    1, 1013, 47840, 455192, 1310354, 1310354, 455192, 47840, 1013,  1
  };
  static const size_t entries = sizeof(table) / sizeof(int);

  size_t k = m + ((n - 1) * n) / 2;

  if (k >= entries)
    {
      errno = EDOM;
      return 0;
    }

  return table[k];
}

static int eulerian(size_t n, size_t m)
{
  if ((n == 0) || (m >= n))
    {
      errno = EDOM;
      return 0;
    }

  return eulerian_lookup(n, m);
}

/* this needs documenting */

static int slice_correction(size_t n, double *c)
{
  double fn = factorial(n);

  c[0] = eulerian(n, 0);

  for (size_t i = 1 ; i < n - 1 ; i++)
    c[i] = c[i - 1] + eulerian(n, i);

  for (size_t i = 0 ; i < n - 1 ; i++)
    c[i] /= fn;

  return errno != 0;
}

/* this needs optimising */

static double lpbsa_leaf(double *s, size_t n, double p)
{
  double
    nps = pnorm(s, n, p),
    t[n], ds[n];

  for (size_t i = 0 ; i < n ; i++)
    t[i] = s[i] / nps;

  for (size_t i = 0 ; i < n ; i++)
    ds[i] = pow(t[i], p - 1);

  double
    n2ds = pnorm(ds, n, 2),
    proj = s[0] * n2ds / dot(ds, s, n),
    scale = pow(nps, n - 1);

  return proj / scale;
}

static double lpbsa_quad(double p,
                         size_t n,
                         size_t m,
                         double *s,
                         double *c,
                         size_t imax,
                         size_t k)
{
  if (k == n)
    {
      if (imax + 2 < n)
        return c[imax] * lpbsa_leaf(s, n, p);

      return lpbsa_leaf(s, n, p);
    }

  double sum = 0.0;

  for (size_t i = 0 ; i <= imax ; i++)
    {
      s[k] = (i + 0.5) / m;
      sum += lpbsa_quad(p, n, m, s, c, i, k + 1);
    }

  return sum;
}

extern double lpbsa(double p, size_t n, size_t m)
{
  return lpbsa_long(p, n, m, NULL);
}

extern double lpbsa_long(double p, size_t n, size_t m, lpbsa_opt_t *opt)
{
  if (opt == NULL)
    {
      lpbsa_opt_t opt = {
	.short_circuit = true
      };
      return lpbsa_long(p, n, m, &opt);
    }

  errno = 0;

  if (n < 2)
    {
      errno = EDOM;
      return BAD_VALUE;
    }

  if ((p < 1) || (isnan(p)))
    {
      errno = EDOM;
      return BAD_VALUE;
    }

  /*
    handle short-circuit cases where there are known formulae
    for the results (this is the default for the short form
    of the function, but we need to be able to turn it off for
    tests).
  */

  if (opt->short_circuit)
    {
      //if (p == 1)
      //  return FIXME;

      if (p == 2)
	return 2 * pow(M_PI, n / 2.0) / tgamma(n / 2.0);

      if (isinf(p))
	return 2 * n * pow(2, n - 1);
    }

  /* slice correction */

  double c[n-2];

  if (slice_correction(n - 1, c) != 0)
    return BAD_VALUE;

  double s[n];

  s[0] = 1;

  /*
    Each projected n-cube surface element has area (1 / m)^(n-1)

    The n-cube has 2n sides, we only calculate the area of (the
    projection of) one of the 2^(n-1) quadrants therein by symmetry

    Each quadrant is symmetric with respect to permutation of the
    n-1 axes, and there are (n-1)! of those
  */

  double
    subarea = 1 / pow(m, n - 1),
    nquads = 2 * n * pow(2, n - 1),
    nperms = factorial(n - 1),
    quad = lpbsa_quad(p, n, m, s, c, m - 1, 1);

  if (errno != 0)
    return BAD_VALUE;

  return subarea * nquads * nperms * quad;
}
