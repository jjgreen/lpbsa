/*
  simple command-line interface to the lpbsa
  function

  Copyright (c) J.J. Green 2018
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#include "lpbsa.c"
#include "options.h"

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  if (info.inputs_num != 4)
    {
      fprintf(stderr, "usage: lpbsa-table <dim> <p-min> <p-max> <samples>\n");
      return EXIT_FAILURE;
    }

  size_t
    p_samples = atoi(info.inputs[3]),
    q_samples = info.quadrature_arg,
    dim = atoi(info.inputs[0]);
  double
    pmin = atof(info.inputs[1]),
    pmax = atof(info.inputs[2]);

  const char *format;
  const char *header = NULL;

  switch (info.format_arg)
    {
    case format_arg_plain:
      format = "%f %f\n";
      break;
    case format_arg_csv:
      header = "p,area";
      format = "%f,%f\n";
      break;
    default:
      fprintf(stderr, "bad output format\n");
      return EXIT_FAILURE;
    }

  if (header != NULL)
    printf("%s\n", header);

  for (size_t i = 0 ; i < p_samples ; i++)
    {
      double
        p = pmin + i * (pmax - pmin) / (p_samples - 1),
        area = lpbsa(p, dim, q_samples);

      if (errno != 0)
        {
          fprintf(stderr, "%s\n", strerror(errno));
          return EXIT_FAILURE;
        }

      printf(format, p, area);
    }

  return EXIT_SUCCESS;
}
